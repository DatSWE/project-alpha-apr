from django.shortcuts import render, redirect
from tasks.forms import CreateTask
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("home")
    else:
        form = CreateTask()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


def task_list(request):
    task = Task.objects.filter(assignee=request.user)
    context = {"task": task}
    return render(request, "tasks/list.html", context)
